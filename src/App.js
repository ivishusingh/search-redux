import React, { Component } from "react";

import "./App.css";
import Navbar from "./components/Navbar";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Form from "./components/Form";
import List from "./components/List";
import Home from "./components/Home";

class App extends Component {
  render() {
    return (
      <div >
        <Navbar />
        <BrowserRouter>
          <Switch>
            <Route exact path="/add" component={Form} />
            <Route exact path="/list" component={List} />
            <Route exact path="/" component={Home} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
