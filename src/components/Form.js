import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Addaction } from "./../store/actions";
import { connect } from "react-redux";

export class Form extends Component {
  state = {
    name: "",
    email: "",
    nameError: "",
    email: ""
  };

  validate = () => {
    let nameError = "";
    let emailError = "";
    if (!this.state.name) {
      nameError = "enter valid name ";
    }
    if (!this.state.email) {
      emailError = "enter valid email";
    }
    if (nameError || emailError) {
      this.setState({
        nameError,
        emailError
      });
      return false;
    }
    return true;
  };
  add = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };
  addUser = e => {
    e.preventDefault();
    const isValid = this.validate();
    if (isValid) {
      this.props.Addaction(this.state.name, this.state.email);

      this.props.history.push("/list");
    }
  };
  render() {
    return (
      <div>
        <div style={{ width: "38rem" }} className="card shadow">
          <div className="card-body">
            <form>
              <div className="form-group">
                <label>Email address</label>
                <input
                  onChange={this.add}
                  type="email"
                  className="form-control"
                  id="email"
                  placeholder="Enter email"
                  required
                />
                {this.state.emailError}
              </div>
              <div className="form-group">
                <label>Name</label>
                <input
                  onChange={this.add}
                  type="text"
                  className="form-control"
                  id="name"
                  placeholder="enter name"
                  required
                />
                {this.state.nameError}
              </div>

              <button
                onClick={this.addUser}
                className="btn btn-outline-success m-1"
              >
                Add User
              </button>

              <Link to="/">
                <button className="btn btn-outline-danger m-1">Cancel</button>
              </Link>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return state;
};

export default connect(
  mapStateToProps,
  { Addaction }
)(Form);
