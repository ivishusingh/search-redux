import React, { Component } from "react";
import { Addaction, DelAction, Edit } from "./../store/actions";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

export class List extends Component {
  state = {
    name: "",
    email: "",
    index: "",
    showInput: false,
    List: this.props.List,
    search: this.props.List
  };
  search = e => {
    const data = this.state.search.filter(item =>
      item.name.toLowerCase().includes(e.target.value.toLowerCase())
    );
    this.setState({ List: data });
  };

  inputHandler = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };
  editItem = i => {
    console.log(i);
    this.setState({ showInput: true, index: i });
  };
  Update = i => {
    this.setState({ showInput: false });
    this.props.Edit(this.state.index, this.state.name, this.state.email);
  };
  Delete = i => {
    console.log(i);
    this.props.DelAction(i);
  };
  render() {
    return (
      <div>
        <div
          style={{ backgroundColor: "#000000" }}
          className="jumbotron jumbotron-fluid App"
        >
          <div className="container">
            <div className="input-group input-group-lg">
              <div className="input-group-prepend">
            
              </div>
              <input
              onChange={this.search}
              placeholder="search here ..."
                type="text"
                className="form-control"
                aria-label="Sizing example input"
                aria-describedby="inputGroup-sizing-lg"
              />
            </div>
          </div>
        </div>
        {this.state.showInput ? (
          <div>
            <label>name</label>
            <input id="name" onChange={this.inputHandler} type="text" />
            <label>email</label>
            <input id="email" onChange={this.inputHandler} type="email" />
            <button
              className=" m-1 btn btn-outline-success"
              onClick={this.Update}
            >
              Update
            </button>{" "}
          </div>
        ) : null}
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col">Actions</th>
              <th scope="col">
                <Link to="/add">
                  <button className="btn btn-warning">Add New</button>
                </Link>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.state.List.map((item, i) => {
              return (
                <tr key={i}>
                  <td>{item.name}</td>
                  <td>{item.email}</td>
                  <td>
                    <Link to="/show">
                      <button
                        onClick={() => this.props.ShowUser(item, i)}
                        className="btn btn-success"
                      >
                        View
                      </button>
                    </Link>
                    <button
                      className="btn btn-outline-danger m-2"
                      onClick={this.Delete.bind(this, i)}
                    >
                      Delete
                    </button>
                    <button
                      className="btn btn-outline-primary"
                      onClick={this.editItem.bind(this, i)}
                    >
                      Edit
                    </button>
                  </td>
                  <td />
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
const mapStateToProps = state => {
  let final = state;
  return final;
};

export default connect(
  mapStateToProps,
  { Addaction, DelAction, Edit }
)(List);
