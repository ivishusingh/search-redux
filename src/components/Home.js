import React from "react";
import { Link } from "react-router-dom";

export default function Home() {
  return (
    <div className="App">
      <div className="card shadow" style={{ width: "28rem" }}>
        <div className="card-body">
          <h5 className="card-title">Welcome to React-Redux App</h5>
          <h6 className="card-subtitle mb-2 text-muted">created by vishal</h6>
          <p className="card-text">Choose what you wanted to....</p>
          <Link to="/add">
            {" "}
            <button className="btn btn-outline-success m-2">Add New</button>
          </Link>
          <Link to="/list">
            <button className="btn btn-outline-warning m-2">Show users</button>
          </Link>
        </div>
      </div>
    </div>
  );
}
